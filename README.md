# Resultado do treinamento

Foram criados dois projetos. O projeto com o nome `helloWorld` para criar algo utilizando os archetypes.
E foi criado outro projeto com o *Spring Initilizr* nest link: [https://start.spring.io/](https://start.spring.io/).

# Slides

[Google Drive](https://drive.google.com/file/d/1kFTqH_FZBXCbk4FfQW_KUkCW7cX6U64Z/view?usp=sharing)

# Obs

- Para compilar um projeto via maven, apenas o `pom.xml` é o suficiente.
- Lembra da pasta `~/.m2/`

# Comandos executados durante a criação do projeto:

```sh
mvn archetype:generate \
    -DgroupId=br.com.neppo \
    -DartifactId=helloWorld \
    -Dversion=0.0.1-SNAPSHOT \
    -DarchetypeGroupId=org.apache.maven.archetypes \
    -DarchetypeArtifactId=maven-archetype-quickstart;
```

```sh
mvn clean package
```

```sh
mvn clean install
```

```sh
java -cp target/helloWorld-0.0.1-SNAPSHOT.jar br.com.neppo.App
```

```sh
mvn clean package -DskipTests
```

```sh
mvn spring-boot:run
```

# Outras coisas:

## Excluindo bibliotecas

```xml
<exclusions>
    <exclusion>
        <groupId>com.example</groupId>
        <artifactId>*</artifactId> <!-- pode ser o id de um artefato -->
    </exclusion>
    <!-- podemos ter mais de uma exclusão --> 
</exclusions>
```

## Comandos úteis

```sh
# Executa os testes
mvn test

# Cria uma arvore de suas dependencias
mvn dependency:tree

# Envia para o repo remoto - requer config
mvn deploy
```

## Links úteis:

- [Build Lifecycle](https://maven.apache.org/guides/introduction/introduction-to-the-lifecycle.html)
- [Plugins](https://maven.apache.org/plugins/)
- [Archetypes](https://maven.apache.org/guides/introduction/introduction-to-archetypes.html)
